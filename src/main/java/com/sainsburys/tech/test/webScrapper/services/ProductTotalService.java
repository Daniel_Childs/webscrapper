package com.sainsburys.tech.test.webScrapper.services;

import com.sainsburys.tech.test.webScrapper.entities.Product;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@Service
public class ProductTotalService {

    double calculateVAT(double grossPrice) {
        double vatAmount = (grossPrice / 1.20);
        double result = grossPrice - vatAmount;
        return new BigDecimal(result).setScale(2, RoundingMode.HALF_UP).doubleValue();
    }

    public  double calculateTotalVAT(List<Product> products) {
        return products.stream().mapToDouble(Product::getUnit_price).map(this::calculateVAT).sum();
    }

    public double calculateTotalGross(List<Product> products) {
        return products.stream().mapToDouble(Product::getUnit_price).sum();
    }
}
