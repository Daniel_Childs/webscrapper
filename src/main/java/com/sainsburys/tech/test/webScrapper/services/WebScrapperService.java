package com.sainsburys.tech.test.webScrapper.services;


import com.sainsburys.tech.test.webScrapper.entities.Product;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InvalidObjectException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class WebScrapperService {

    @Value("${berries.cherries.currants.url}")
    private String targetUrl;

    @Value("${sainsburies.base.uri}")
    private String sainsburiesBaseURL;

    private final String hrefBaseURLToStrip = "../../../../../..";

    private final String titleClassName = "productTitleDescriptionContainer";

    private final String tableRow0ClassName = "tableRow0";
    private final String pricePerUnitClassName = "pricePerUnit";
    private final String descriptionProductTextClassName = "productText";

    private Document document;

    public Document connect(String targetUrl) throws IOException {
        try {
            document = Jsoup.connect(targetUrl).get();
        } catch (IOException e) {
            throw new IOException(e);
        }
        return document;
    }

    public List<String> getProductsURIS(String url) throws IOException {
        document = connect(url);

        if (!document.baseUri().equalsIgnoreCase(targetUrl)) {
            throw new InvalidObjectException("The Document base URI does not match the target URI");
        }
        Elements productNameAndPromotions = document.getElementsByClass("productNameAndPromotions");

        return productNameAndPromotions.select("a").stream()
                .map(element -> element.attributes().get("href"))
                .map(this::concatURL)
                .collect(Collectors.toList());
    }

    public String concatURL(String url) {
        String formattedURL = url.split(hrefBaseURLToStrip)[1];
        return sainsburiesBaseURL.concat(formattedURL);
    }

    public Product scrapeProductPage(String productPageUrl) throws IOException {
        Document document = connect(productPageUrl);

        String title = scrapeProductTitle(document, titleClassName);
        int kcalPer100g = scrapeKcalPer100G(document, tableRow0ClassName);
        double pricePerUnit = scrapePricePerUnit(document, pricePerUnitClassName);
        String description = scrapePageForDescription(document);
        Product product = new Product(title, kcalPer100g, pricePerUnit, description);

        return product;
    }


    public String scrapeProductTitle(Document document, String className) {
        Elements titleElement = document.getElementsByClass(className);
        return titleElement.eachText().get(0);

    }

    public int scrapeKcalPer100G(Document document, String className) {

        Elements nutritionTableTableRow0 = document.getElementsByClass(className);
        if (!nutritionTableTableRow0.isEmpty()) {
            String tableData = nutritionTableTableRow0.first().text();
            int indexOfK = tableData.indexOf('k');
            return Integer.parseInt(tableData.substring(0, indexOfK));
        }
        // a decision has been made to not find the kcal value for https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-cherry-punnet-200g-468015-p-44.html
        // due to time restraints and the fact this table is differnet from others.
        return 0;
    }

    public double scrapePricePerUnit(Document document, String className) {
        Elements pricePerUnitElements = document.getElementsByClass(className);
        String pricePerUnit = pricePerUnitElements.first().text();
        List<Character> chars = pricePerUnit.chars().mapToObj(c -> (char) c).collect(Collectors.toList());
        List<Character> digitsAndDecimals = chars.stream().filter(character -> Character.isDigit(character) || character.equals('.')).collect(Collectors.toList());
        StringBuilder stringbuilder = new StringBuilder();
        digitsAndDecimals.forEach(stringbuilder::append);
        return Double.parseDouble(stringbuilder.toString());
    }

    public String scrapePageForDescription(Document document) {
        Elements descriptionElements = document.getElementsByClass(descriptionProductTextClassName);
        if (!descriptionElements.isEmpty()) {
            String description = descriptionElements.first().children().first().text();
            if (description.equalsIgnoreCase("description")) {
                description = descriptionElements.first().children().eachText().get(1);
            }
            return description;
        }

        System.out.println("Unable to find a product description");
        return "";
    }

    public List<Product> scrapeProducts(List<String> productPageUrls) throws InvalidObjectException {
        if (productPageUrls.isEmpty()){
            throw new InvalidObjectException("There are no productPageUrls");
        }

        List<Product> products = new ArrayList<>();
        productPageUrls.forEach(url -> {
            try {
                products.add(scrapeProductPage(url));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        return products;
    }

}
