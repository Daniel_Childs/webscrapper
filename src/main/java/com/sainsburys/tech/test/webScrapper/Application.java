package com.sainsburys.tech.test.webScrapper;

import com.sainsburys.tech.test.webScrapper.entities.Product;
import com.sainsburys.tech.test.webScrapper.entities.ProductTotal;
import com.sainsburys.tech.test.webScrapper.services.JSONBuilderService;
import com.sainsburys.tech.test.webScrapper.services.ProductTotalService;
import com.sainsburys.tech.test.webScrapper.services.WebScrapperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootApplication
public class Application implements CommandLineRunner {

    @Value("${berries.cherries.currants.url}")
    private String targetURL;

    @Autowired
    WebScrapperService webScrapperService;

    @Autowired
    ProductTotalService productTotalService;

    @Autowired
    JSONBuilderService jsonBuilderService;

    public static void main(String[] args) {
        new SpringApplicationBuilder(Application.class).web(WebApplicationType.NONE).run(args);
    }

    @Override
    public void run(String... args) throws Exception {
        List<String> targetURLS = webScrapperService.getProductsURIS(targetURL);
        List<Product> products = webScrapperService.scrapeProducts(targetURLS);
        double total = productTotalService.calculateTotalGross(products);
        double totalVAT = productTotalService.calculateTotalVAT(products);
        ProductTotal productTotal = new ProductTotal(total, totalVAT);
        Map<String, Object> objectMap = new HashMap<>();
        objectMap.put("Results", products);
        objectMap.put("Total", productTotal);
        String json = jsonBuilderService.createProductJSON(objectMap);
        System.out.println(json);
    }
}
