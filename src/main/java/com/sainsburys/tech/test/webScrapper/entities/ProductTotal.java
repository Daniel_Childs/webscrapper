package com.sainsburys.tech.test.webScrapper.entities;

public class ProductTotal {

    private double total;
    private double vat;

    public ProductTotal(double total, double vat) {
        this.total = total;
        this.vat = vat;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getVat() {
        return vat;
    }

    public void setVat(double vat) {
        this.vat = vat;
    }
}
