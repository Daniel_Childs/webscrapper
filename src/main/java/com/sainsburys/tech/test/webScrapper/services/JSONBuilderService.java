package com.sainsburys.tech.test.webScrapper.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Map;

@Service
public class JSONBuilderService {

    ObjectMapper objectMapper = new ObjectMapper();

    public String createProductJSON(Map<String, Object> objects) throws IOException {
        return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(objects);

    }

}
