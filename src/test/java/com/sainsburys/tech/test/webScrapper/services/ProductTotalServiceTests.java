package com.sainsburys.tech.test.webScrapper.services;

import com.sainsburys.tech.test.webScrapper.entities.Product;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;

@SpringBootTest
public class ProductTotalServiceTests {

    @Autowired
    ProductTotalService productTotalService;

    @DisplayName("Tests for Calculate VAT")
    @Nested
    class TestsForCalculateVAT {
        @ParameterizedTest
        @CsvSource(value = {
                "0.29, 1.75",
                "0.25, 1.50",
                "0.54, 3.25",
                "0.42, 2.50",
                "0.58, 3.50",
                "0.46, 2.75",
                "0.67, 4.00",
                "0.17, 1.00",
                "0.33, 2.00",
                "0.46, 2.75"})
        void calculateVatShouldReturnTheCorrectAmount(double expected, double grossPrice) {
            Assertions.assertEquals(expected, productTotalService.calculateVAT(grossPrice));
        }

        @Test
        void calculateVatShouldReturnTheZeroWhenPriceIsZero() {
            double expected = 0;
            Assertions.assertEquals(expected, productTotalService.calculateVAT(0));
        }

    }

    @DisplayName("Tests for Calculating total gross and total VAT")
    @Nested
    class testsForTotalGrossANDTotalVat {
        Product product1 = new Product();
        Product product2 = new Product();
        Product product3 = new Product();
        Product product4 = new Product();

        @BeforeEach()
        void setUp() {
            product1.setUnit_price(1.75);
            product2.setUnit_price(1.75);
            product3.setUnit_price(1.75);
            product4.setUnit_price(1.75);
        }

        @Test
        void calculateTotalVATShouldReturnTheCorrectAmountWhenTheGrossPriceIsTheSame() {
            List<Product> productList = Arrays.asList(product1, product2, product3, product4);
            Assertions.assertEquals(1.16, productTotalService.calculateTotalVAT(productList));
        }

        @Test
        void calculateTotalVATShouldReturnTheCorrectAmountWhenTheGrossPriceIsTheDifferent() {
            product1.setUnit_price(1.50);
            product2.setUnit_price(3.25);
            product3.setUnit_price(1.00);
            product4.setUnit_price(2.75);
            List<Product> productList = Arrays.asList(product1, product2, product3, product4);
            Assertions.assertEquals(1.42, productTotalService.calculateTotalVAT(productList));
        }

        @Test
        void calculateTotalGrossShouldReturnTheCorrectAmountWhenTheGrossPricesAreTheeSame() {
            List<Product> productList = Arrays.asList(product1, product2, product3, product4);
            Assertions.assertEquals(7, productTotalService.calculateTotalGross(productList));
        }

        @Test
        void calculateTotalVATShouldReturnTheCorrectAmountWhenTheGrossPricesAreDifferent() {
            product1.setUnit_price(1.50);
            product2.setUnit_price(3.25);
            product3.setUnit_price(1.00);
            product4.setUnit_price(2.75);
            List<Product> productList = Arrays.asList(product1, product2, product3, product4);
            Assertions.assertEquals(8.50, productTotalService.calculateTotalGross(productList));
        }

    }
}
