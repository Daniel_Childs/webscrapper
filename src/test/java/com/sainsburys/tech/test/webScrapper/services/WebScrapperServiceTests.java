package com.sainsburys.tech.test.webScrapper.services;


import com.sainsburys.tech.test.webScrapper.entities.Product;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.io.IOException;
import java.io.InvalidObjectException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class WebScrapperServiceTests {

    @Value("${berries.cherries.currants.url}")
    private String targetUrl;

    @MockBean
    Jsoup jsoup;

    @Autowired
    WebScrapperService webScrapperService;

    @DisplayName("Tests for connect()")
    @Nested
    class WebScrapperServiceTestsConnect {
        @Test
        void connectShouldReturnTheCorrectDocument() throws IOException {
            Document document = webScrapperService.connect(targetUrl);
            Element expectedElement = document.getElementById("resultsHeading");
            String expected = expectedElement.ownText();
            Assertions.assertEquals(expected, "Berries, cherries & currants (17 products available)");
        }

        @Test
        void connectShouldThrownAnIOExceptionWhenTheTargetURiIsMalformed() throws IOException {
            assertThrows(IllegalArgumentException.class, () -> webScrapperService.connect("malformedURI"));
        }

        @Test
        void connectShouldThrownAnIOExceptionWhenTheTargetUriIsNotFound() throws IOException {
            assertThrows(IOException.class, () -> webScrapperService.connect("http://google.co.uk/test"));
        }
    }

    @Nested
    @DisplayName("Tests for getProductsURIS()")
    class GetProductURLTests {

        @Test
        void getProductURISShouldReturnTheCorrectListStrings() throws IOException {
            String expectedURL = "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-british-strawberries-400g.html";
            List<String> results = webScrapperService.getProductsURIS(targetUrl);
            Assertions.assertEquals(expectedURL, results.get(0));
        }

        @Test
        void getProductURISShouldThrowAnExpectionWhenThereAreNoHREFSInTheDocument() throws URISyntaxException, IOException {
            InvalidObjectException exception = assertThrows(InvalidObjectException.class, () ->
                    webScrapperService.getProductsURIS("https://www.google.co.uk/"));
            Assertions.assertEquals("The Document base URI does not match the target URI", exception.getMessage());
        }
    }

    @Nested
    @DisplayName("Tests for concatURL()")
    class ConcatURLTests {

        private String hrefBaseURLToStrip = "../../../../../../shop/gb/groceries/berries-cherries-currants/sainsburys-british-strawberries-400g.html";

        @Test
        void shouldReturnTheCorrectString() {
            String expectedURL = "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-british-strawberries-400g.html";
            String result = webScrapperService.concatURL(hrefBaseURLToStrip);
            Assertions.assertEquals(result, expectedURL);
        }
    }

    @Nested
    @DisplayName(("Tests for methods scrape elements from pages"))
    class elementScrapingMethodTests {
        String titleClassName = "productTitleDescriptionContainer";
       String kcalPer100gClassName = "tableRow0";
       String pricePerUnitClassName = "pricePerUnit";

        @ParameterizedTest
        @CsvSource(value = {
                "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-strawberries--taste-the-difference-300g.html | Sainsbury's Strawberries, Taste the Difference 300g",
                "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-cherry-punnet-200g-468015-p-44.html | Sainsbury's Cherry Punnet 200g",
                "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-raspberries--taste-the-difference-150g.html | Sainsbury's Raspberries, Taste the Difference 150g" },
                delimiter = '|')
        void scrapeProductTitleShouldReturnTheCorrectTitle(String url, String expected) throws IOException {
            Document document = webScrapperService.connect(url);
            String result = webScrapperService.scrapeProductTitle(document, titleClassName);
            Assertions.assertEquals(expected, result);
        }

        @ParameterizedTest
        @CsvSource(value = {
                "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-strawberries--taste-the-difference-300g.html | 33",
                "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-cherry-punnet--taste-the-difference-250g.html | 48 ",
                "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-raspberries--taste-the-difference-150g.html | 32" },
        delimiter = '|')
        void scrapeKcalPer100GShouldReturnTheCorrectPrice(String url, int expected) throws IOException {
            Document document = webScrapperService.connect(url);
            int result = webScrapperService.scrapeKcalPer100G(document, kcalPer100gClassName);
            Assertions.assertEquals(expected, result);
        }

        @ParameterizedTest
        @CsvSource(value = {
                "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-cherry-punnet-200g-468015-p-44.html",
                "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-blackcurrants-150g.html",
                "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-british-cherry---strawberry-pack-600g.html" })
        void scrapeKcalPer100GShouldReturnZeroWenItCannotFindTheCorrectTableDataElemeent(String url) throws IOException {
            Document document = webScrapperService.connect(url);
            int result = webScrapperService.scrapeKcalPer100G(document, kcalPer100gClassName);
            Assertions.assertEquals(0, result);
        }

        @ParameterizedTest
        @CsvSource(value = {
                "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-cherry-punnet-200g-468015-p-44.html | 1.50",
                "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-blackcurrants-150g.html | 1.75",
                "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-british-cherry---strawberry-pack-600g.html | 4.00" },
                delimiter = '|')
        void scrapePricePerUnitShouldReturnTheCorrectPrice(String url, double expected) throws IOException {
            Document document = webScrapperService.connect(url);
            double result = webScrapperService.scrapePricePerUnit(document, pricePerUnitClassName);
            Assertions.assertEquals(expected, result);
        }

        @ParameterizedTest()
        @CsvSource(value = {
                "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-cherries-350g.html | by Sainsbury's Family Cherry Punnet",
                "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-british-strawberries-400g.html | by Sainsbury's strawberries",
                "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-blueberries--so-organic-150g.html |  So Organic blueberries",
                "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-british-cherry---strawberry-pack-600g.html | British Cherry & Strawberry Mixed Pack",
                "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-mixed-berry-twin-pack-200g-7696255-p-44.html | Mixed Berries",
                "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-redcurrants-150g.html | by Sainsbury's redcurrants",
                "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-cherry-punnet--taste-the-difference-250g.html | Cherry Punnet",
                "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-blackcurrants-150g.html |Union Flag",
                "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-blackberries--tangy-150g.html | by Sainsbury's blackberries",
                "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-strawberries--taste-the-difference-300g.html | Ttd strawberries",
                "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-cherry-punnet-200g-468015-p-44.html | Cherries",
                "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-mixed-berries-300g.html | by Sainsbury's mixed berries",
                "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-british-strawberries-400g.html | by Sainsbury's strawberries",
                "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-blueberries-200g.html | by Sainsbury's blueberries ",
                "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-raspberries-225g.html | by Sainsbury's raspberries",
                "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-blackberries--sweet-150g.html | by Sainsbury's blackberries",
        },delimiter = '|')
        void shouldReturnTheCorrectDescription(String url, String expected) throws IOException {
            Document document = webScrapperService.connect(url);
            String result = webScrapperService.scrapePageForDescription(document);
            assertEquals(expected, result );
        }
        @ParameterizedTest
        @CsvSource(value = {
                "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-mixed-berry-twin-pack-200g-7696255-p-44.html | Mixed Berries",
                "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-british-cherry---strawberry-pack-600g.html | British Cherry & Strawberry Mixed Pack",
                "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-cherry-punnet-200g-468015-p-44.html | Cherries"
        },delimiter = '|')
        void shouldReturnTheFirstLinOfADescriptionWhenTheFirstLineHsACLassANameOfMemo(String url, String expected) throws IOException {
            Document document = webScrapperService.connect(url);
            String result = webScrapperService.scrapePageForDescription(document);
            assertEquals(expected, result);
        }
    }

    @Nested
    @DisplayName("Tests for methods scrape product page ")
    class scrapeProductPageTests {

        @Test
        void shouldReturnTheCorrectProductWhenPageContainsAllRequiredInformation() throws IOException {
            Product expected = new Product();
            expected.setTitle("Sainsbury's Strawberries 400g");
            expected.setDescription("by Sainsbury's strawberries");
            expected.setUnit_price(1.75);
            expected.setKcal_per_100g(33);
            Product result = webScrapperService.scrapeProductPage("https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-british-strawberries-400g.html");
            assertEquals(expected.getTitle(), result.getTitle());
            assertEquals(expected.getDescription(), result.getDescription());
            assertEquals(expected.getUnit_price(), result.getUnit_price());
            assertEquals(expected.getKcal_per_100g(), result.getKcal_per_100g());
        }

        @ParameterizedTest
        @CsvSource(value = {
                "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-british-cherry---strawberry-pack-600g.html | Sainsbury's British Cherry & Strawberry Pack 600g | British Cherry & Strawberry Mixed Pack | 4.00",
                "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-blackcurrants-150g.html | Sainsbury's Blackcurrants 150g | Union Flag |1.75 |",
                "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-cherry-punnet-200g-468015-p-44.html | Sainsbury's Cherry Punnet 200g | Cherries |1.50 " }
        ,delimiter = '|')
        void shouldReturnTheCorrectProductWhenPageDoesNotContainRequiredInformation(String url, String title, String description, double price) throws IOException {
            Product expected = new Product();
            expected.setTitle(title);
            expected.setDescription(description);
            expected.setUnit_price(price);
            Product result = webScrapperService.scrapeProductPage(url);
            assertEquals(expected.getTitle(), result.getTitle());
            assertEquals(expected.getDescription(), result.getDescription());
            assertEquals(expected.getUnit_price(), result.getUnit_price());
            assertEquals(expected.getKcal_per_100g(), result.getKcal_per_100g());
        }

       @Test
        void scrapeProductsShouldReturnAListOfProducts() throws InvalidObjectException {
            Product product1 = new Product("Sainsbury's Cherries 400g", 52,2.50,"by Sainsbury's Family Cherry Punnet");
            Product product2 = new Product("Sainsbury's Strawberries 400g", 33, 1.75, "by Sainsbury's strawberries");
            Product product3 = new Product("Sainsbury's Blueberries, SO Organic 150g",  45, 2.00, "So Organic blueberries");
            List<Product> expected = new ArrayList<>();
            expected.add(product1);
            expected.add(product2);
            expected.add(product3);
            List<Product> results = webScrapperService.scrapeProducts(Arrays.asList(
                    "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-cherries-350g.html",
                    "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-british-strawberries-400g.html",
                    "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-blueberries--so-organic-150g.html"));
            assertEquals(expected.size(), results.size());
           assertEquals( expected.get(0).getTitle(),results.get(0).getTitle());
           assertEquals(expected.get(1).getTitle(), results.get(1).getTitle());
           assertEquals(expected.get(2).getTitle(), results.get(2).getTitle());
       }

       @Test
       void scrapeProductsShouldThrowAnErrorWhenInputIsEmpty() {
            Assertions.assertThrows(InvalidObjectException.class, () -> {
                webScrapperService.scrapeProducts(Collections.EMPTY_LIST);
           });
       }


    }
}
