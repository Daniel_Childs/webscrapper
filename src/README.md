This application uses Java 8, SpringBoot, JSoup, Jackson, Junit5 and Maven Compiler Plugin.

In order to run this application you will need Java 1.8 and Maven installed on your machine.

In order to run this application you need to run the following command:
    
    mvn spring-boot:run
       
In order to run the tests run will need to run the following command:
    
    mvn test
    
   
Unfortunatly I was unable to emit the kcal_per_100g field from the JSON String due to running out of time. In order to achieve this functionality I would have done either of the following:

    1. Create a List<String> from the  JSON String seperating each string by a comma. 
    2. Remove the lines that match "kcal_per_100g" : 0" and return a new String 
    3. Create a String using the "Total" JSON Node from the String generated by the JSONBuilderService.createProductJSON method
    4. Add the two json Strings to a JSON Array and print this to the console 
    
    OR
    
    1. Create a method that writes a JSON String from a List<Product> (as a pose from a Map<String, Object>
    2. Iterate over the json nodes add remove each field that where the kcal_per_100g is 0
    This would require creating a seperate method to write the ProductTotal as a json string and adding this to the JSON Array
    
     
   